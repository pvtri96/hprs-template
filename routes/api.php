<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'auth:api'], function() {
	
	Route::group(['prefix' => 'auth'], function() {

		Route::get('current-user', [
			'as' => 'api.auth.current_user',
			'uses' => 'DashboardController@getCurrentUser'
		]);

	});

	Route::group(['prefix' => 'users'], function() {

		Route::get('list', [
			'as' => 'api.users.list',
			'uses' => 'Users\ManagementController@getAllUsers'
		]);

	});

	Route::group(['prefix' => 'employees'], function() {

		Route::get('list', [
			'as' => 'api.employees.employee-info',
			'uses' => 'Employee\EmployeeController@getAllEmployees'
		]);

		Route::get('detail/{employee_SSN}', [
			'as' => 'api.employees.employee-info.detail',
			'uses' => 'Employee\EmployeeController@getEmployeeDetail'
		]);

		Route::post('delete', [
			'as' => 'api.employees.employee-info.delete',
			'uses' => 'Employee\EmployeeController@delete'
		]);

		Route::post('create', [
			'as' => 'api.employees.employee-info.create',
			'uses' => 'Employee\EmployeeController@create'
		]);

		Route::get('pay-rate', [
			'as' => 'api.employees.total-earning',
			'uses' => 'Employee\TotalEarningController@getTotalEarnings'
		]);

		Route::get('pay-rate/detail/{employee_SSN}', [
			'as' => 'api.employees.total-earning.detail',
			'uses' => 'Employee\TotalEarningController@getTotalEarningDetail'
		]);

		Route::get('vacation-days', [
			'as' => 'api.employees.vacation-days',
			'uses' => 'Employee\VacationDaysController@getVacationDays'
		]);

		Route::get('vacation-days/detail/{employee_SSN}', [
			'as' => 'api.employees.vacation-days.detail',
			'uses' => 'Employee\VacationDaysController@getVacationDayDetail'
		]);
	});

});
