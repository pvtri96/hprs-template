<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function() {

	Route::get('/', [
	    'as' => 'view.dashboard',
	    'uses' => function(){
	        return view('partials.master');
	    }
	]);
	
	Route::get('vacation-day-details', [
	    'as' => 'view.vacation-day-details',
	    'uses' => function(){
	        return view('partials.vacation-day-details');
	    }
	]);
});

Route::group(['prefix' => 'auth'], function() {

	Route::get('login', [
		'as' => 'view.auth.login',
		'uses' => 'Auth\LoginController@getLogin'
	]);

	Route::post('login', [
		'as' => 'view.auth.login',
		'uses' => 'Auth\LoginController@postLogin'
	]);

	Route::get('logout', [
		'as' => 'view.auth.logout',
		'uses' => 'Auth\LoginController@logout'
	]);
});

Route::get('/test', function () {
    return view('test.connection');
});

Route::get('/views/load', [
    'as' => 'views.load',
    'uses' => function (Illuminate\Http\Request $request) {
        $path = $request->get('path');
        return view($path)->render();
    }
]);
