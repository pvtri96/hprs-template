const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.js('resources/assets/js/vendor.js', 'public/bundled/js')
	.js('resources/assets/js/angular.js', 'public/bundled/js')
    .sass('resources/assets/sass/vendor.scss', 'public/bundled/css/vendor.css')
	.sass('resources/assets/sass/dashboard.scss', 'public/bundled/css/dashboard.css')
    .sass('resources/assets/sass/auth.scss', 'public/bundled/css/auth.css');

mix.browserSync({
	proxy: 'localhost:8000',
	files: [
		'resources/views/**/*.blade.php',
		'resources/lang/**/*.php',
		'public/bundled/**/*.*',
	]
});