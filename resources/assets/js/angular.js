require('./components/parseView.js');
require('./dashboard/app.js');
require('./dashboard/routes.js');
require('./components/angular-template-bracket.js');
require('angular-utils-pagination/dirPagination.js');
require('@cgross/angular-notify/angular-notify.js');


/**
 * Controllers
 */
require('./dashboard/controllers/main.controller.js');
require('./dashboard/controllers/users.controller.js');
require('./dashboard/controllers/total-earning.controller.js');
require('./dashboard/controllers/total-earning-detail.controller.js');
require('./dashboard/controllers/employee.controller.js');
require('./dashboard/controllers/employee-detail.controller.js');
require('./dashboard/controllers/creating-employee.controller.js');
require('./dashboard/controllers/vacation-days.controller.js');
require('./dashboard/controllers/vacation-days-detail.controller.js');
require('./dashboard/controllers/dashboard.controller.js');

/**
 * Services
 */
require('./dashboard/services/auth-http.service.js');
require('./dashboard/services/employee.service.js');
require('./dashboard/services/calculator.service.js');

/**
 * Directives
 */
require('./dashboard/directives/delete-model.directive.js');
require('./dashboard/directives/loading-snippet.directive.js');