window.parseView = (path) => {
    let url = '/views/load';
    let params = {
        path: path
    };
    params = $.param(params);
    let parseUrl = url + "?" + params;
    return parseUrl;
}
