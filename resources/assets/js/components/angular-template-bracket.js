(function() {
    'use strict';

    angular
        .module('HumanAndPayrollModule')
        .config(BracketConfig);

    BracketConfig.$inject = ['$interpolateProvider'];

    /* @ngInject */
    function BracketConfig($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }
})();
