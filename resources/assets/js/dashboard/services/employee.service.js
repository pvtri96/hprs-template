(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .service('EmployeeService', EmployeeService);

	EmployeeService.$inject = ['$authHttp'];

	function EmployeeService($http) {
		/**
		 * Initialize
		 */
		this.init = () => {
			let init = {
				genders: [
					{
						name: 'Male',
						code: 1
					},
					{
						name: 'Female',
						code: 0
					}
				],
				shareholderStatus: [
					{
						name: 'Shareholder',
						code: 1
					},
					{
						name: 'Non-shareholder',
						code: 0
					}
				],
				jobTypes: [
					{
						name: 'Full-time',
						code: 1
					},
					{
						name: 'Part-time',
						code: 2
					},
				],
				ethnicities: ['Kinh', 'HMong', 'Muong', 'Meo', 'Tay'],
				departments: ['Operator', 'Team', 'Quet rac', 'Security'],
				locations: ['A', 'B', 'C']
			};

			return init;
		};

		/**
		 * Employees
		 */
		this.getAll = () => {
			return $http.get('/api/employees/list');
		}

		this.getOne = (SSN) => {
			let path = '/api/employees/detail/' + SSN;
			return $http.get(path);
		};

		this.getModel = (employees, SSN) => {
            let employee = employees.filter((employee) => {
                return employee.SSN == SSN;
            })
            return employee[0];
        };

		this.getFullname = (first_name, last_name, middle_name) => {
			return last_name + ' ' + middle_name + ' ' + first_name;
		};

		this.getGender = (gender) => {
			if (gender == 1) {
				return 'Male';
			}
			return 'Female';
		};

		this.delete = (employee) => {
			return $http.post('/api/employees/delete', employee);
		};

		this.create = (employee) => {
			return $http.post('/api/employees/create', employee);
		};

		/**
		 * Employee with Pay rate
		 */
		this.getAllWithPayRate = () => {
			return $http.get('/api/employees/pay-rate');
		};

		this.getOneWithPayRate = (SSN) => {
			let $path = '/api/employees/pay-rate/detail/' + SSN;
			return $http.get($path);
		};

		/**
		 * Vacation days of employees
		 */
		this.getVacationDays = () => {
			return $http.get('/api/employees/vacation-days');
		};

		this.getVacationDayDetail = (SSN) => {
			let $path = '/api/employees/vacation-days/detail/' + SSN;
			return $http.get($path);
		};
	}
})();