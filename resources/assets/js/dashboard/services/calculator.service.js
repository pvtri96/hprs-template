(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .service('CalculatorService', CalculatorService);

	CalculatorService.$inject = [];

	function CalculatorService() {
		this.getTotalEarning = (employee) => {
			let payType = employee.pay_rate.pay_type;
			let payValue = employee.pay_rate.value;
			let payAmount = employee.pay_rate.pay_amount;
			let tax = employee.pay_rate.tax_percentage;
			let hoursPerWeek = employee.job_histories[0].hours_per_week;

			if (payType == 1) {
				let normal_paid = (payValue * payAmount) * (1 - tax/100);
				let OT = hoursPerWeek - 40;

				if (OT > 0)
					return normal_paid + ((payValue * payAmount) / 20 / 8) * OT * 1.5;
				else
					return normal_paid;
			} else {
				let normal_paid = (payAmount * hoursPerWeek) * (1 - tax/100);
				let OT = hoursPerWeek - 25;

				if (OT > 0)
					return normal_paid + (payAmount * OT * 1.5);
				else
					return normal_paid;
			}
		}
	}
})();