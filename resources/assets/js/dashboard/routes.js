(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .config(RouteConfig);

	RouteConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

	function RouteConfig($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('dashboard', {
				url: '/',
				templateUrl: parseView('partials.dashboard'),
				controller: 'DashboardController'
			})
			.state('total_earnings', {
				abstract: true,
				url: '/total-earning',
				template: '<ui-view/>'
			})
			.state('number_of_vacation_days', {
				abstract: true,
				url: '/vacation',
				template: '<ui-view/>'
			})
			.state('birthday_in_month', {
				abstract: true,
				url: '/birthday-in-month',
				template: '<ui-view/>'
			})
			.state('employees_management', {
				abstract: true,
				url: '/employee',
				template: '<ui-view/>'
			})
			.state('users_management', {
				abstract: true,
				url: '/users',
				template: '<ui-view/>'
			})
		angular.forEach(Auth.permissions, (permission) => {
			switch (permission.state_name) {
				case 'total_earnings': 
					$stateProvider
						.state('total_earnings.list', {
							url: '/list',
							templateUrl: parseView('partials.total-earning.main.list'),
							controller: 'TotalEarningController'
						})
						.state('total_earnings.details', {
							url: '/details?employeeSSN',
							templateUrl: parseView('partials.total-earning.main.detail'),
							controller: 'TotalEarningDetailController'
						});
					break;

				case 'number_of_vacation_days': 
					$stateProvider
						.state('number_of_vacation_days.list', {
							url: '/list',
							templateUrl: parseView('partials.vacation-days.main.list'),
							controller: 'VacationDaysController'
						})
						.state('number_of_vacation_days.details', {
							url: '/detail?employeeSSN',
							templateUrl: parseView('partials.vacation-days.main.detail'),
							controller: 'VacationDayDetailController'
						});
					break;

				case 'birthday_in_month':
					$stateProvider
						.state('birthday_in_month.list', {
							url: '/list',
							templateUrl: parseView('partials.birthday-in-month.main.list'),
							//controller: 'birthdaysManagementController'
						})
						.state('birthday_in_month.detail', {
							url: '/detail?userId',
							templateUrl: parseView('partials.birthday-in-month.main.detail'),
							//controller: 'EmployeeDetailController'
						})
					break;
				
				case 'employees_management': 
					$stateProvider
						.state('employees_management.list', {
							url: '/list',
							templateUrl: parseView('partials.employee.main.list'),
							controller: 'EmployeeController'
						})
						.state('employees_management.detail', {
							url: '/detail?employeeSSN',
							templateUrl: parseView('partials.employee.main.detail'),
							controller: 'EmployeeDetailController'
						})
						.state('employees_management.create', {
							url: '/create',
							templateUrl: parseView('partials.employee.main.create'),
							controller: 'CreatingEmployeeController'
						});
					break;
				
				case 'users_management': 
					$stateProvider
						.state('users_management.list', {
							url: '/list',
							templateUrl: parseView('partials.users.main.list'),
							controller: 'UsersManagementController'
						})
						.state('users_management.detail', {
							url: '/detail?userId',
							templateUrl: parseView('partials.users.main.detail'),
							//controller: 'EmployeeDetailController'
						})
						.state('users_management.create', {
							url: '/create',
							templateUrl: parseView('partials.users.main.create'),
							//controller: 'CreatingEmployeeController'
						});
					break;
			}
		})
	}

	angular
        .module('HumanAndPayrollModule')
        .run(['$rootScope', ($rootScope) => {
            $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
                $rootScope.isLoading = true;
            });
            $rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
                $rootScope.isLoading = false;
            });
        }])
})();
