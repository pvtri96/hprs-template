(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .directive('deleteModel', deleteModel);

	function deleteModel() {
		return {
			restrict: 'EA',
			scope: {
				model: '=',
				label: '@'
			},
			templateUrl: parseView('directives.delete-model'),
			link: (scope, el, attrs) => {
				scope.$watch('model', (model) => {
					if (model) {
						scope.modelName = model[scope.label]
					}
				});

				scope.delete = () => {
					let mainScope = angular.element('#main').scope();
					mainScope.delete(scope.model);
					$('.deleted').attr('data-dismiss', 'modal');
				}
			}
		}
	}
})();