(function() {
	'use strict';

	angular
		.module('HumanAndPayrollModule', [
	  		'ui.router',
	  		'angularUtils.directives.dirPagination',
	  		'cgNotify',
	  	]);
})();