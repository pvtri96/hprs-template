(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('EmployeeController', EmployeeController);

	EmployeeController.$inject = ['$scope', '$rootScope', '$state', 'EmployeeService', 'notify'];

	function EmployeeController($scope, $rootScope, $state, EmployeeService, notify) {
		$scope.isShowBirthday = true;

		let init = EmployeeService.init();
		$scope.genders = init.genders;
		$scope.ethnicities = init.ethnicities;
		$scope.jobTypes = init.jobTypes;
		$scope.departments = init.departments;
		$scope.shareholderStatus = init.shareholderStatus;

		$scope.employees = [];

		EmployeeService.getAll().then((response) => {
			$scope.employees = response.data.data;
			angular.forEach($scope.employees, (employee) => {
				employee.full_name = EmployeeService.getFullname(employee.first_name, employee.last_name, employee.middle_name);
			});
		});

		$scope.goToEditorPage = (employee_SSN) => {
			$state.go('employees_management.detail', {employeeSSN: employee_SSN});
			$rootScope.isUpdate = true;
		};

		$scope.getEmployeeToDelete = (employee) => {
			$scope.deleted_employee = EmployeeService.getModel($scope.employees, employee.SSN);
		}

		$scope.delete = (deleted_employee) => {
            $rootScope.isLoading = true;
			EmployeeService.delete(deleted_employee).then((response) => {
				$scope.employees = $scope.employees.filter((employee) => {
					return employee.employee_id != deleted_employee.employee_id;
				})

           		$rootScope.isLoading = false;
				
				notify({
					message: 'Delete successful.',
					classes: 'success'
				})
			});
		};

		/**
		 * Filter
		 */
		$scope.filterBy = (name, value) => {
			$scope.setFilter = (employee) => {
				if (name == 'department')
					return employee.job_histories[0].department === value.toString();

				if (name == 'pay_type')
					return employee.pay_rate.pay_type === value;

				return employee[name] === value.toString();
			}
		};

		$scope.resetFilter = () => {
			$scope.setFilter = (employee) => {
				return employee.employee_id != null;
			}	
		}

		/**
         * Pagination
         */
        $scope.quantities = [5, 10, 20, 50, 100];
        $scope.dirPaginateTemplate = parseView('directives.dir-pagination');
        $scope.viewBy = $scope.quantities[0];
        $scope.currentPage = 1;
        $scope.pageSize = $scope.viewBy;

        // Handle view by quantity
        $scope.setItemsPerPage = () => {
            $scope.pageSize = $scope.viewBy;
            $scope.currentPage = 1;
        };
	}
})();