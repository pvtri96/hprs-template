(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('EmployeeDetailController', EmployeeDetailController);

	EmployeeDetailController.$inject = ['$scope', '$stateParams', 'EmployeeService'];

	function EmployeeDetailController($scope, $stateParams, EmployeeService) {
		EmployeeService.getOne($stateParams.employeeSSN).then((response) => {
			$scope.employee = response.data.data;
		})
	}
})();