(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('TotalEarningController', TotalEarningController);

	TotalEarningController.$inject = ['$scope', '$rootScope', 'EmployeeService', 'CalculatorService'];

	function TotalEarningController($scope, $rootScope, EmployeeService, CalculatorService) {
		let init = EmployeeService.init();
		$scope.genders = init.genders;
		$scope.ethnicities = init.ethnicities;
		$scope.jobTypes = init.jobTypes;
		$scope.departments = init.departments;
		$scope.shareholderStatus = init.shareholderStatus;

		EmployeeService.getAllWithPayRate().then((response) => {
			$scope.employeesWithPayrate = response.data.data;
			angular.forEach($scope.employeesWithPayrate, (employee) => {
				employee.full_name = EmployeeService.getFullname(employee.first_name, employee.last_name, employee.middle_name);
				employee.total_earning = CalculatorService.getTotalEarning(employee);
			});
			
		});

		/**
		 * Filter
		 */
		$scope.filterBy = (name, value) => {
			$scope.setFilter = (employee) => {
				if (name == 'department')
					return employee.job_histories[0].department === value.toString();
				
				if (name == 'pay_type')
					return employee.pay_rate.pay_type === value;
				
				return employee[name] === value.toString();
			}
		};

		$scope.resetFilter = () => {
			$scope.setFilter = (employee) => {
				return employee.employee_id != null;
			}	
		}

		/**
         * Pagination
         */
        $scope.quantities = [5, 10, 20, 50, 100];
        $scope.dirPaginateTemplate = parseView('directives.dir-pagination');
        $scope.viewBy = $scope.quantities[0];
        $scope.currentPage = 1;
        $scope.pageSize = $scope.viewBy;

        // Handle view by quantity
        $scope.setItemsPerPage = () => {
            $scope.pageSize = $scope.viewBy;
            $scope.currentPage = 1;
        };
	}
})();