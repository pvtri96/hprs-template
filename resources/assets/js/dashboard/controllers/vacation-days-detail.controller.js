(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('VacationDayDetailController', VacationDayDetailController);

	VacationDayDetailController.$inject = ['$scope', '$stateParams', 'EmployeeService'];

	function VacationDayDetailController($scope, $stateParams, EmployeeService) {
		EmployeeService.getVacationDayDetail($stateParams.employeeSSN).then((response) => {
			$scope.employee = response.data.data;
		})
	}
})();