(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('CreatingEmployeeController', CreatingEmployeeController);

	CreatingEmployeeController.$inject = ['$scope', 'EmployeeService', '$timeout', 'notify'];

	function CreatingEmployeeController($scope, EmployeeService, $timeout, notify) {
		let init = EmployeeService.init();
		$scope.genders = init.genders;
		$scope.casuals = init.casuals;
		$scope.departments = init.departments;
		$scope.positions = init.positions;
		$scope.locations = init.locations;
		$scope.shareholderStatus = init.shareholderStatus;

		$scope.tabs = {
			personal: {
				name: 'personal',
				isChanged: true
			},

			job: {
				name: 'job',
				isChanged: false
			},
			
			employment: {
				name: 'employment',
				isChanged: false
			},

			benefits: {
				name: 'benefits',
				isChanged: false
			}
		};


		$scope.changeTab = (name_tag) => {
			Object.keys($scope.tabs).map((key) => {
				if (name_tag === $scope.tabs[key].name)
					$scope.tabs[key].isChanged = true;
				else 
					$scope.tabs[key].isChanged = false;
			})
		}

		$scope.warning = () => {
			notify({
				message: 'This feature is not yet implemented.',
				classes: 'warning'
			})
		}
	}
})();