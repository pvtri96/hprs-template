(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('UsersManagementController', UsersManagementController);

	UsersManagementController.$inject = ['$authHttp', '$scope'];

	function UsersManagementController($http, $scope) {
		$http.get('/api/users/list').then(response => {
			$scope.users = response.data.data;
			console.log($scope.users)
		})

		/**
         * Pagination
         */
        $scope.quantities = [5, 10, 20, 50, 100];
        $scope.dirPaginateTemplate = parseView('directives.dir-pagination');
        $scope.viewBy = $scope.quantities[0];
        $scope.currentPage = 1;
        $scope.pageSize = $scope.viewBy;

        // Handle view by quantity
        $scope.setItemsPerPage = () => {
            $scope.pageSize = $scope.viewBy;
            $scope.currentPage = 1;
        };
	}
})();