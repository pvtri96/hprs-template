(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('MainController', MainController);

	MainController.$inject = ['$rootScope', '$authHttp', 'notify'];

	function MainController($rootScope, $http, notify) {
		$rootScope.Auth = {
			permissions: Auth.permissions
		};

		notify.config({
			templateUrl: parseView('directives.notify'),
			position: 'right',
			duration: '3500',
			startTop: 60
		});

		$http.get('/api/auth/current-user').then(response => {
			$rootScope.Auth.user = response.data.data;
		});

		$rootScope.$on('$stateNotFound', (event, unfoundState, fromState, fromParams) => {
			notify({
				message: 'You don\'t have permission to access this form. Please ask administrator for your permission.',
				classes: 'warning'
			})
		})
	}
})();