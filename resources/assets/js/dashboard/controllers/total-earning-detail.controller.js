(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('TotalEarningDetailController', TotalEarningDetailController);

	TotalEarningDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'EmployeeService', 'CalculatorService'];

	function TotalEarningDetailController($scope, $rootScope, $stateParams, EmployeeService, CalculatorService) {
		EmployeeService.getOneWithPayRate($stateParams.employeeSSN).then((response) => {
			$scope.totalEarningDetail = response.data.data;
			$scope.totalEarningDetail.total_earning = CalculatorService.getTotalEarning(response.data.data);
		});
	}
})();