(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('DashboardController', DashboardController);

	DashboardController.$inject = ['$scope', 'notify'];

	function DashboardController($scope, notify) {
		$scope.view = () => {
			notify({
				message: 'This feature is not yet implemented.',
				classes: 'warning'
			})
		}
	}
})();