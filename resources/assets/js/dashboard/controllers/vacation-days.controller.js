(function() {
	'use strict';

	angular
	  .module('HumanAndPayrollModule')
	  .controller('VacationDaysController', VacationDaysController);

	VacationDaysController.$inject = ['$scope', 'EmployeeService', '$timeout'];

	function VacationDaysController($scope, EmployeeService, $timeout) {
		$scope.employees = [];
		$scope.total_vacation_days = 0;

		let init = EmployeeService.init();
		$scope.genders = init.genders;
		$scope.ethnicities = init.ethnicities;
		$scope.jobTypes = init.jobTypes;
		$scope.departments = init.departments;
		$scope.shareholderStatus = init.shareholderStatus;

		EmployeeService.getVacationDays().then((response) => {
			$scope.employees = response.data.data;
			angular.forEach($scope.employees, (employee) => {
				employee.full_name = EmployeeService.getFullname(employee.first_name, employee.last_name, employee.middle_name);
			});
			$scope.totalOfVacationDays($scope.employees);
		});

		$scope.totalOfVacationDays = (employees) => {
			$scope.total_vacation_days = 0;
			angular.forEach(employees, (employee) => {
				$scope.total_vacation_days += employee.vacation_days;
			});
		};

		$scope.vacationDaysCal = () => {
			$timeout(() => {
				$scope.totalOfVacationDays($scope.filteredEmployee);
			})
		};


		/**
		 * Filter
		 */
		$scope.filterBy = (name, value) => {
			$scope.setFilter = (employee) => {
				if (name == 'department')
					return employee.job_histories[0].department === value.toString();
				
				if (name == 'pay_type')
					return employee.pay_rate.pay_type === value;
				
				return employee[name] === value.toString();
			}

			$timeout(() => {
				$scope.totalOfVacationDays($scope.filteredEmployee);
			})
		};

		$scope.resetFilter = () => {
			$scope.setFilter = (employee) => {
				return employee.employee_id != null;
			}

			$timeout(() => {
				$scope.totalOfVacationDays($scope.filteredEmployee);
			})	
		}


		/**
         * Pagination
         */
        $scope.quantities = [5, 10, 20, 50, 100];
        $scope.dirPaginateTemplate = parseView('directives.dir-pagination');
        $scope.viewBy = $scope.quantities[0];
        $scope.currentPage = 1;
        $scope.pageSize = $scope.viewBy;

        // Handle view by quantity
        $scope.setItemsPerPage = () => {
            $scope.pageSize = $scope.viewBy;
            $scope.currentPage = 1;
        };
	}
})();