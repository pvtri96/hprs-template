<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
		<link rel="stylesheet" type="text/css" href="/bundled/css/vendor.css">
		<link rel="stylesheet" type="text/css" href="/bundled/css/auth.css">
	</head>
	<body>
		<div class="background">
            <div class="caption">
                <h1>Human & Payroll System</h1>
            </div>
    		<div class="container login-container">
    			<form class="col-xs-5" action="{{ route('view.auth.login') }}" method="POST">
    				<div class="login-form">
    					{{ csrf_field() }}
    					<div class="warning-message">
    						@php
    							if (count($errors) > 0) {
    								foreach ($errors->all() as $error) {
    									echo $error;
    								}
    							}
    							$value = Session::get('message');
    							if (isset($value)) {
    								echo $value;
    							}
    						@endphp
    					</div>
    					<div class="form-group">
    						<input type="text" name="email" id="email" class="form-control" placeholder="Email">
    					</div>
    					<div class="form-group">
    						<input type="password" name="password" id="password" class="form-control"
    								placeholder="Password">
    					</div>
    					<div class="form-group submit clearfix">
    						<button class="btn btn-primary ">Login</button>
    					</div>
    				</div>
    			</form>
    		</div>
        </div>
	</body>
</html>
