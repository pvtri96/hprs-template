<div class="form-information">
    <label class="label-content">
        <span class="title"><b>ID:</b></span>
        <span ng-bind="employee.employee_id"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Firstname:</b></span>
        <span ng-bind="employee.first_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Lastname:</b></span>
        <span ng-bind="employee.last_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Middlename:</b></span>
        <span ng-bind="employee.middle_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Shareholder status:</b></span>
        <span ng-bind="employee.shareholder_status == 1 ? 'Yes' : 'No'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Gender:</b></span>
        <span ng-bind="employee.gender == 1 ? 'Male' : 'Female'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Ethnicity:</b></span>
        <span ng-bind="employee.ethnicity"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Job Type:</b></span>
        <span ng-bind="employee.pay_rate.pay_type == 1 ? 'Full-time' : 'Part-time'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Department:</b></span>
        <span ng-bind="employee.job_histories.department"</span>
    </label>
    <label class="label-content">
        <span class="title"><b>Position:</b></span>
        <span ng-bind="employee.job_histories.division"</span>
    </label>
    <label class="label-content">
        <span class="title"><b>Vacation days:</b></span>
        <span ng-bind="employee.vacation_days"></span>
    </label>
</div>
