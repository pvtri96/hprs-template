<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Number of vacation days</li>
            </ul>
        </div>
            <div class="body-content">
                <div class="navbar body-navbar col-md-12">
                    <h1 class="col-md-6">Number of Vacation days</h1>
                    @include('components.filter')
                </div>
                <table class="table table-bordered table-hover list">
                    <thead>
                        <tr>
                            @include('components.properties')
                            <th>Vacation days</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="employee in (filteredEmployee = (employees | filter: search_input | filter: setFilter)) | itemsPerPage: pageSize"
                            current-page="currentPage"
                            pagination-id="itemsPagination"
                            ui-sref="number_of_vacation_days.details({employeeSSN: employee.SSN})">
                            <td ng-bind="employee.employee_id"></td>
                            <td ng-bind="employee.full_name"></td>
                            <td ng-bind="employee.shareholder_status == 1 ? 'Yes' : 'No'"></td>
                            <td ng-bind="employee.gender == 1 ? 'Male' : 'Female'"></td>
                            <td ng-bind="employee.ethnicity"></td>
                            <td ng-bind="employee.pay_rate.pay_type == 1 ? 'Full-time' : 'Part-time'"></td>
                            <td ng-bind="employee.job_histories[0].department"></td>
                            <td ng-bind="employee.job_histories[0].division"></td>
                            <td ng-bind="employee.vacation_days"></td>
                        </tr>
                        <tr>
                            <td colspan="8"><strong>Total of vacation days</strong></td>
                            <td><strong ng-bind="total_vacation_days" ng-bind="total_vacation_days"></strong></td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-md-12 tool-bar">
                    @include('components.pagination')
                </div>
            </div>
        </div>
    </div>
</div>
