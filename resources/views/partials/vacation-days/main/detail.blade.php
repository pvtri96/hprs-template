<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a ui-sref="number_of_vacation_days.list">Number of Vacation days</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Detail</li>
            </ul>
        </div>
        <div class="body-content detail">
            <div class="navbar body-navbar col-md-12">
                <h1 class="col-md-12">Number of Vacation days Details</h1>
            </div>
            <div class="col-md-12 row">
                @include('partials.vacation-days.components.detail-form')
            </div>
        </div>
    </div>
</div>
