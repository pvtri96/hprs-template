<div class="item sub-form">
    <div class="main-form form-horizontal row">
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="firstname">Firstname</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="firstname" name="firstname"
                    placeholder="Firstname" ng-model="employee.first_name">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="lastname">Lastname</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="lastname" name="lastname"
                    placeholder="Lastname" ng-model="employee.last_name">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="middle-initial">Middle Initial</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="middle-initial" name="middle-initial"
                    placeholder="Middle Initial"
                        ng-model="employee.middle_initial">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="birthday">Day of birth</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="date" id="birthday" name="birthday"
                    ng-model="employee.birthday">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="address1">Address 1</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="address1" name="address1"
                    placeholder="Address" ng-model="employee.address1">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="address2">Address 2</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="address2" name="address2"
                    placeholder="Address" ng-model="employee.address2">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="city">City</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="city" name="city" placeholder="City"
                ng-model="employee.city">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="state">State</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="state" name="state" placeholder="State"
                ng-model="employee.state">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="zip">Zip</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="zip" name="zip" placeholder="Zip"
                ng-model="employee.zip">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="email">Email</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="email" name="email" placeholder="Email"
                ng-model="employee.email">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="phone">Phone number</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="phone" name="phone"
                    placeholder="Phone number" ng-model="employee.phone">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="SSN">Social Security number</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="SSN" name="SSN"
                    placeholder="Social Security number" ng-model="employee.SSN">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="driver-license">Driver License</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="driver-license" name="driver-license"
                    placeholder="Driver License" ng-model="employee.driver_license">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="marital-status">Marital Status</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="marital-status" name="marital-status"
                    placeholder="Marital Status" ng-model="employee.marital_status">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="gender">Gender</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="gender" name="gender"
                    ng-options="gender.code as gender.name for gender in genders"
                        ng-model="employee.gender">
                    <option value="">Choose gender</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="shareholder-status">Shareholder Status</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="shareholder-status" name="shareholder-status"
                    ng-options="status.code as status.name for status in shareholderStatus"
                        ng-model="employee.shareholder_status">
                    <option value="">Choose shareholder status</option>
                </select>
            </div>
        </div>
    </div>
</div>
