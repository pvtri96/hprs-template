<div class="item sub-form">
    <div class="main-form form-horizontal row">
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="department">Department</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="department" name="department"
                    ng-options="department as department for department in departments"
                    ng-model="employee.department">
                    <option value="">Choose department</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="division">Division</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="division" name="division"
                    placeholder="Division">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="start-date">Start Date</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="date" id="start-date" name="start-date"
                    ng-model="employee.start_date">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="end-date">End Date</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="date" id="end-date" name="end-date"
                    ng-model="employee.end_date">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="job-title">Job title</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="job-title" name="job-title"
                    placeholder="Job title">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="supervisor">Supervisor</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="supervisor" name="supervisor"
                    placeholder="Supervisor">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="job-category">Job category</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="job-category" name="job-category">
                    <option value="">Choose location</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="location">Location</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="location" name="location"
                    ng-options="location as location for location in locations"
                    ng-model="employee.location">
                    <option value="">Choose location</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="department-code">Department code</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="number" id="department-code" name="department-code"
                    placeholder="Department code">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="salary-type">Salary type</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="salary-type" name="salary-type">
                    <option value="">Choose salary type</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="pay-period">Pay period</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="pay-period" name="pay-period">
                    <option value="">Choose pay period</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="hours-per-week">Hours per week</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="number" id="hours-per-week" name="hours-per-week"
                    placeholder="Hours per week">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="hazardous-training">Hazardous training</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="hazardous-training" name="hazardous-training"
                    placeholder="Hazardous training">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="casual">Casual</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="casual" name="casual"
                    ng-options="casual as casual for casual in casuals"
                    ng-model="employee.casual">
                    <option value="">Choose casual</option>
                </select>
            </div>
        </div>

        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="position">Position</label>
            <div class="form-input col-sm-8">
                <select class="form-control" id="position" name="position"
                    ng-options="position as position for position in positions"
                    ng-model="employee.position">
                    <option value="">Choose position</option>
                </select>
            </div>
        </div>
    </div>
</div>
