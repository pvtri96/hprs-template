<div class="item sub-form">
    <div class="main-form form-horizontal row">
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="employee-status">Employment Status</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="employee-status" name="employee-status"
                    placeholder="Employment Status">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="hire-date">Hire date</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="date" id="hire-date" name="hire-date"
                    placeholder="Hire date">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="workers-comp-code">Workers Comp Code</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="number" id="workers-comp-code" name="workers-comp-code"
                    placeholder="Workers Comp Code">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="termination-date">Termination date</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="date" id="termination-date" name="termination-date"
                    placeholder="Termination date">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="rehire-date">Rehire date</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="date" id="rehire-date" name="rehire-date"
                    placeholder="Rehire date">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="last-review-date">Last review date</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="date" id="last-review-date" name="last-review-date"
                    placeholder="Last review date">
            </div>
        </div>
    </div>
</div>
