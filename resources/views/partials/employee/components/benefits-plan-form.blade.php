<div class="item sub-form">
    <div class="main-form form-horizontal row">
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="plan-name">Plan name</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="plan-name" name="plan-name"
                    placeholder="Plan name">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="deductable">Deductable</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="text" id="deductable" name="deductable"
                    placeholder="Deductable">
            </div>
        </div>
        <div class="form-group col-sm-7">
            <label class="control-label col-sm-4" for="percemtage-coPay">Percentage CoPay</label>
            <div class="form-input col-sm-8">
                <input class="form-control" type="number" id="percemtage-coPay" name="percemtage-coPay"
                    placeholder="Percentage CoPay">
            </div>
        </div>
    </div>
    @include("partials.employee.components.creating-action")
</div>
