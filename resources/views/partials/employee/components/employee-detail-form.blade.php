<div class="form-information form-horizontal">
    <label class="label-content">
        <span class="title"><b>ID:</b></span>
        <span ng-bind="employee.employee_id"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Firstname:</b></span>
        <span ng-bind="employee.first_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Lastname:</b></span>
        <span ng-bind="employee.last_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Middle name:</b></span>
        <span ng-bind="employee.middle_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Birthday:</b></span>
        <span class="title">2/8/1970</span>
    </label>
    <label class="label-content">
        <span class="title"><b>Address 1:</b></span>
        <span ng-bind="employee.address1"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Address 2:</b></span>
        <span ng-bind="employee.address2"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>City:</b></span>
        <span ng-bind="employee.city"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>State:</b></span>
        <span ng-bind="employee.state"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Zip:</b></span>
        <span ng-bind="employee.zip"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Email:</b></span>
        <span ng-bind="employee.email"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Phone number:</b></span>
        <span ng-bind="employee.phone_number"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Social security number:</b></span>
        <span ng-bind="employee.SSN"></span>
    </label>
</div>
<div class="form-payrate">
    <label class="label-content">
        <span class="title"><b>Driver License:</b></span>
        <span ng-bind="employee.drivers_license"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Marital Status:</b></span>
        <span ng-bind="employee.marital_status"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Shareholder Status:</b></span>
        <span class="title" ng-bind="employee.shareholder_status == 1 ? 'Yes' : 'No'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Gender:</b></span>
        <span ng-bind="employee.gender == 1 ? 'Male' : 'Female'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Ethnicity:</b></span>
        <span ng-bind="employee.ethnicity"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Job Type:</b></span>
        <span class="title" ng-bind="employee.pay_rate.pay_type == 1 ? 'Full-time' : 'Part-time'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Department:</b></span>
        <span class="title" ng-bind="employee.job_histories.department"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Position:</b></span>
        <span class="title" ng-bind="employee.job_histories.division"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Location:</b></span>
        <span class="title" ng-bind="employee.job_histories.location"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Start date:</b></span>
        <span class="title">1/7/2000</span>
    </label>
    <label class="label-content">
        <span class="title"><b>End date:</b></span>
        <span class="title">Unknow</span>
    </label>
</div>
