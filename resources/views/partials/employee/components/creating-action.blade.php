<div class="tool-bar col-md-12 create-page">
    <div class="action">
        <div class="button-form create-page">
            <button type="button" class="btn-cancel btn btn-danger" ng-click="warning()">
                <i class="fa fa-ban" aria-hidden="true"></i>
                <span>Cancel</span>
            </button>
        </div>
        <div class="button-form create-page">
            <button type="button" class="btn-refresh btn btn-warning" ng-click="warning()">
                <i class="fa fa-refresh" aria-hidden="true"></i>
                <span>Refresh</span>
            </button>
        </div>
        <div class="button-form create-page">
            <button class="btn-add btn btn-primary" ng-click="warning()">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
                <span>Add</span>
            </button>
        </div>
    </div>
</div>