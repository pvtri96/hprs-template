<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a ui-sref="employees_management.list">Employee's Information</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Create an employee</li>
            </ul>
        </div>
        <div class="body-content">
            <div class="navbar body-navbar col-md-12">
                <h1 class="col-md-12">Create New Employee</h1>
            </div>
            <div class="main-content">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active" ng-click="changeTab('personal')">
                        <a href="personal-information" aria-controls="personal-information" role="tab" data-toggle="tab">Personal Information</a>
                    </li>
                    <li role="presentation" ng-click="changeTab('job')">
                        <a href="job-information" aria-controls="job-information" role="tab" data-toggle="tab">Job Information</a>
                    </li>
                    <li role="presentation" ng-click="changeTab('employment')">
                        <a href="employment-information" aria-controls="employment-information" role="tab" data-toggle="tab">Employment Information</a>
                    </li>
                    <li role="presentation" ng-click="changeTab('benefits')">
                        <a href="benefits-plan" aria-controls="benefits-plan" role="tab" data-toggle="tab">Benefits plan</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="personal-information" 
                        ng-class="tabs.personal.isChanged ? 'active' : ''">
                        @include('partials.employee.components.personal-info-form')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="job-information"
                        ng-class="tabs.job.isChanged ? 'active' : ''">
                        @include('partials.employee.components.job-info-form')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="employment-information"
                        ng-class="tabs.employment.isChanged ? 'active' : ''">
                        @include('partials.employee.components.employment-info-form')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="benefits-plan"
                        ng-class="tabs.benefits.isChanged ? 'active' : ''">
                        @include('partials.employee.components.benefits-plan-form')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
