<div class="page-content-wrapper" id="main">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Employee's Information</li>
            </ul>
        </div>
            <div class="body-content">
                <div class="navbar body-navbar col-md-12">
                    <h1 class="col-md-6">Employee's Information</h1>
                    @include('components.filter')
                </div>
                <table class="table table-bordered table-hover list">
                    <thead>
                        <tr>
                            @include('components.properties')
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="employee in employees | filter: search_input | filter: setFilter | itemsPerPage: pageSize"
                            current-page="currentPage"
                            pagination-id="itemsPagination">
                            <td ng-bind="employee.employee_id"></td>
                            <td ng-bind="employee.full_name"
                                ui-sref="employees_management.detail({employeeSSN: employee.SSN})"></td>
                            <td>2/8/1970</td>
                            <td ng-bind="employee.shareholder_status == 1 ? 'Yes' : 'No'"></td>
                            <td ng-bind="employee.gender == 1 ? 'Male' : 'Female'"></td>
                            <td ng-bind="employee.ethnicity"></td>
                            <td ng-bind="employee.pay_rate.pay_type == 1 ? 'Full-time' : 'Part-time'"></td>
                            <td ng-bind="employee.job_histories[0].department"></td>
                            <td ng-bind="employee.job_histories[0].division"></td>
                            <td class="action">
                                <a class="btn btn-sm btn-danger"
                                    ng-click="getEmployeeToDelete(employee)"
                                    data-toggle="modal" data-target="#deleteModal">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                                <a class="btn btn-sm btn-warning"
                                    ng-click="goToEditorPage(employee.SSN)">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <delete-model model="deleted_employee" label="full_name"></delete-model>
                <div class="tool-bar col-md-12">
                    <div class="add-btn">
                        <a ui-sref="employees_management.create">
                            <button class="btn-create btn btn-primary">
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                <span>Add New Employee</span>
                            </button>
                        </a>
                    </div>
                    @include('components.pagination')
                </div>
            </div>
        </div>
    </div>
</div>
