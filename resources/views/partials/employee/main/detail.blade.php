<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a ui-sref="employee.list">Employee's Information</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Employee's Information Details</li>
            </ul>
        </div>
        <div class="body-content detail">
            <div class="navbar body-navbar col-md-12">
                <h1 class="col-md-12">Employee's Information Details</h1>
            </div>
            <div class="col-md-12 row">
                @include('partials.employee.components.employee-detail-form')
            </div>
        </div>
    </div>
</div>
