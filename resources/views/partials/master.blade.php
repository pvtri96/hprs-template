<!DOCTYPE html>
<html lang="en" ng-app="HumanAndPayrollModule" ng-controller="MainController">
    <head>
        <meta charset="utf-8" />
        <title>HPRS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="#1 selling multi-purpose bootstrap admin theme sold in themeforest marketplace packed with angularjs, material design, rtl support with over thausands of templates and ui elements and plugins to power any type of web applications including saas and admin dashboards. Preview page of Theme #1 for statistics, charts, recent events and reports"
            name="description" />
        <meta content="" name="author" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/bundled/css/vendor.css" rel="stylesheet" type="text/css" />
        <link href="/bundled/css/dashboard.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" ng-cloak>
        <div class="page-wrapper">
            @include('components.navbar')
            <div class="clearfix"> </div>
            <div class="page-container">
                @include('components.sidebar')
                @include('components.container')
            </div>
        </div>
        <div class="quick-nav-overlay"></div>
        <script src="/bundled/js/vendor.js" type="text/javascript"></script>
        <script type="text/javascript">
            window.Auth = {
                api_token: '{!! Auth::user()['api_token'] !!}',
                permissions: {!! json_encode(App\Http\Controllers\DashboardController::getPermissions(Auth::user()['user_id'])) !!}
            };
            angular.forEach(Auth.permissions, (permission, index) => {
                permission.state_name = permission.feature_name.toLowerCase().split(" ").join("_");
            });
        </script>
        <script src="/bundled/js/angular.js" type="text/javascript"></script>
    </body>
</html>
