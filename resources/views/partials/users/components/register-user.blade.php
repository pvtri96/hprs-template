<div class="item sub-form">
    <div class="main-form">
        <div class="form-group">
            <div class="caption">
                <label class="control-label" for="first_name">First name</label>
            </div>
            <div class="form-input">
                <input class="form-control" type="text" id="first_name" name="first_name"
                        placeholder="Firstname" ng-model="employee.first_name">
            </div>
        </div>
        <div class="form-group">
            <div class="caption">
                <label class="control-label" for="last_name">Last name</label>
            </div>
            <div class="form-input">
                <input class="form-control" type="text" id="last_name" name="last_name"
                        placeholder="Lastname" ng-model="employee.last_name">
            </div>
        </div>
        <div class="form-group">
            <div class="caption">
                <label class="control-label" for="email">Email</label>
            </div>
            <div class="form-input">
                <input class="form-control" type="text" id="email" name="email"
                        placeholder="example@example.com"
                        ng-model="employee.middle_initial">
            </div>
        </div>
        <div class="form-group">
            <div class="caption">
                <label class="control-label" for="password">Password</label>
            </div>
            <div class="form-input">
                <input class="form-control" type="text" id="password" name="password"
                        placeholder="Middle Initial"
                        ng-model="employee.middle_initial">
            </div>
        </div>
    </div>
</div>
