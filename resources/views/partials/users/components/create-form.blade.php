<div class="item active sub-form">
    <div class="caption">
        <label>Clone user from an existed user</label>
    </div>
    <div class="main-form">
        @include('partials.users.components.clone-user')
    </div>
</div>
<div class="item sub-form">
    <div class="caption">
        <label>Register a new user</label>
    </div>
    <div class="main-form">
        @include('partials.users.components.register-user')
    </div>
</div>
