<div class="item sub-form">
    <div class="main-form">
        <div class="form-group">
            <div class="caption">
                <label class="control-label" for="selected_user">Pick a user</label>
            </div>
            <div class="form-input">
                <select class="form-control" ng-model="employee.selected_user"></select>
            </div>
        </div>
        <div class="form-group">
            <div class="caption">
                <label class="control-label" for="email">Email</label>
            </div>
            <div class="form-input">
                <input class="form-control" type="text" id="email" name="email"
                    placeholder="example@example.com"
                    ng-model="employee.middle_initial">
            </div>
        </div>
        <div class="form-group">
            <div class="caption">
                <label class="control-label" for="password">Password</label>
            </div>
            <div class="form-input">
                <input class="form-control" type="text" id="password" name="password"
                    placeholder="Middle Initial"
                    ng-model="employee.middle_initial">
            </div>
        </div>
    </div>
</div>
