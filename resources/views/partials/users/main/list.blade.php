<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Users management</li>
            </ul>
        </div>
            <div class="body-content">
                <div class="navbar body-navbar col-md-12">
                    <h1 class="col-md-6">Users management</h1>
                    <div class="col-md-3 number-of-table-form">
                        <p class="col-md-8">Number of rows: </p>
                        <div class="col-md-4">
                            <select class="form-control number-of-table" id="inlineFormCustomSelect"
                                ng-options="quantity as quantity for quantity in quantities"
                                ng-model="viewBy"
                                ng-change="setItemsPerPage()">
                            </select>
                        </div>
                    </div>
                    <div class="input-group search-box col-md-3">
                        <input type="text" class="form-control" placeholder="Search..."
                                ng-model="search_input">
                    </div>
                </div>
                <table class="table table-bordered table-hover list">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Department
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li ng-click="resetFilter()">
                                            <a>All</a>
                                        </li>
                                        <li ng-repeat="gender in genders" ng-click="filterBy('gender', gender)">
                                            <a ng-bind="gender == 1 ? 'Male' : 'Female'"></a>
                                        </li>
                                    </ul>
                                </div>
                            </th>
                            <th>Joined at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="user in users | filter: search_input | itemsPerPage: pageSize"
                            current-page="currentPage"
                            pagination-id="itemsPagination"
                            ui-sref="total_earnings.details({userSSN: user.SSN})">
                            <td ng-bind="user.user_id"></td>
                            <td ng-cloak>[[user.first_name]] [[user.last_name]]</td>
                            <td ng-bind="user.email"></td>
                            <td ng-bind="user.department"></td>
                            <td ng-bind="user.created_at"></td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-md-12 tool-bar">
                    <div class="col-md-3">
                        <a ui-sref="users_management.create">
                            <button class="btn-create btn btn-primary">
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                <span>Add New User</span>
                            </button>
                        </a>
                    </div>
                    <div class="pagenumber col-md-4">
                        <div class="pagenumber col-md-4">
                            <div
                                dir-pagination-controls
                                boudary-link="true"
                                pagination-id="itemsPagination"
                                template-url="/views/load?path=directives.dir-pagination">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
