
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a ui-sref="employee.list">Employee's Information</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Create an employee</li>
            </ul>
        </div>
        <div class="body-content">
            <div class="navbar body-navbar col-md-12">
                <h1 class="col-md-12">Create New Employee</h1>
            </div>
            <div class="main-content">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="clone-user" aria-controls="clone-user" role="tab" data-toggle="tab">Clone User</a></li>
                    <li role="presentation"><a href="register-user" aria-controls="register-user" role="tab" data-toggle="tab">Register User</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="clone-user">@include('partials.users.components.clone-user')</div>
                    <div role="tabpanel" class="tab-pane" id="register-user">  @include('partials.users.components.register-user')</div>
                </div>
            </div>
            <div class="tool-bar col-md-12 create-page">
                <div class="action">
                    <div class="button-form create-page">
                        <button type="button" class="btn-cancel btn btn-danger">
                            <i class="fa fa-ban" aria-hidden="true"></i>
                            <span>Cancel</span>
                        </button>
                    </div>
                    <div class="button-form create-page">
                        <button type="button" class="btn-refresh btn btn-warning">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                            <span>Refresh</span>
                        </button>
                    </div>
                    <div class="button-form create-page">
                        <button class="btn-add btn btn-primary" ng-click="create(employee)">
                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                            <span>Add</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
