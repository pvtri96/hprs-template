<div class="form-information">
    <label class="label-content">
        <span class="title"><b>ID:</b></span>
        <span ng-bind="totalEarningDetail.employee_id"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Firstname:</b></span>
        <span ng-bind="totalEarningDetail.first_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Lastname:</b></span>
        <span ng-bind="totalEarningDetail.last_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Shareholder status:</b></span>
        <span ng-bind="totalEarningDetail.shareholder_status"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Gender:</b></span>
        <span ng-bind="totalEarningDetail.gender == 1 ? 'Male' : 'Female'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Ethnicity:</b></span>
        <span ng-bind="totalEarningDetail.ethnicity"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Job Type:</b></span>
        <span ng-bind="totalEarningDetail.pay_rate.pay_type == 1 ? 'Full-time' : 'Part-time'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Department:</b></span>
        <span ng-bind="totalEarningDetail.job_histories[0].department"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Position:</b></span>
        <span ng-bind="totalEarningDetail.job_histories[0].division"></span>
    </label>
</div>
<div class="form-payrate">
    <label class="label-content">
        <span class="title"><b>Hour/week:</b></span>
        <span ng-bind="totalEarningDetail.job_histories[0].hours_per_week"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Pay Type Level:</b></span>
        <span ng-bind="totalEarningDetail.pay_rate.pay_type_level"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Pay Value (VND):</b></span>
        <span ng-bind="totalEarningDetail.pay_rate.value"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Pay Amount (VND):</b></span>
        <span ng-bind="totalEarningDetail.pay_rate.pay_amount"></span>
    </label>
    <label class="label-content total-earning">
        <span><b>Total Earning (vnd):</b></span>
        <span ng-bind="totalEarningDetail.total_earning"></span>
    </label>
</div>
