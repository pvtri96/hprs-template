<div class="page-content-wrapper" id="main">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a ui-sref="dashboard">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>Birthday in month</li>
            </ul>
        </div>
            <div class="body-content">
                <div class="navbar body-navbar col-md-12">
                    <h1 class="col-md-6">Birthday in month</h1>
                    @include('components.filter')
                </div>
                <table class="table table-bordered table-hover list">
                    <thead>
                        <tr>
                            @include('partials.birthday-in-month.components.birthday-properties')
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <div class="col-md-12 tool-bar">
                    @include('components.pagination')
                </div>
            </div>
        </div>
    </div>
</div>
