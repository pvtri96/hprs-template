<th>ID</th>
<th>Name</th>
<th>Birthday</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Shareholder
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#">A</a></li>
            <li><a href="#">B</a></li>
            <li><a href="#">C</a></li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Gender
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="gender in genders" ng-click="filterBy('gender', gender)">
                <a ng-bind="gender == 1 ? 'Male' : 'Female'"></a>
            </li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Ethnicity
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="ethnicity in ethnicities" ng-click="filterBy('ethnicity', ethnicity)">
                <a ng-bind="ethnicity"></a>
            </li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Casual
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="casual in casuals">
                <a ng-bind="casual"></a>
            </li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Department
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="department in departments">
                <a ng-bind="department"></a>
            </li>
        </ul>
    </div>
</th>
<th>Position</th>
