<div class="form-information">
    <label class="label-content">
        <span class="title"><b>ID:</b></span>
        <span ng-bind="employee.employee_id"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Firstname:</b></span>
        <span ng-bind="employee.first_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Lastname:</b></span>
        <span ng-bind="employee.last_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Middle:</b></span>
        <span ng-bind="employee.middle_name"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Birthday:</b></span>
        <span ng-bind="employee.birthday"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Shareholder status:</b></span>
        <span ng-bind="employee.shareholder_status"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Gender:</b></span>
        <span ng-bind="employee.gender == 1 ? 'Male' : 'Female'"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Ethnicity:</b></span>
        <span ng-bind="employee.ethnicity"></span>
    </label>
    <label class="label-content">
        <span class="title"><b>Casual:</b></span>
        <span Full-time</span>
    </label>
    <label class="label-content">
        <span class="title"><b>Department:</b></span>
        <span Security</span>
    </label>
    <label class="label-content">
        <span class="title"><b>Position:</b></span>
        <span Leader</span>
    </label>
</div>
