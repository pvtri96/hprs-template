<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <div class="cancel-btn">
                    <button class="close" data-dismiss="modal">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <p>Do you want to delete <strong ng-bind="modelName"></strong> employee?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">No</button>
                <button class="btn btn-danger deleted" ng-click="delete()">Yes</button>
            </div>
        </div>
    </div>
</div>
