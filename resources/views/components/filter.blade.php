<div class="number-of-table-form">
    <p>Number of rows: </p>
    <div>
        <select class="form-control number-of-table" id="inlineFormCustomSelect"
                ng-options="quantity as quantity for quantity in quantities"
                ng-model="viewBy"
                ng-change="setItemsPerPage()">
        </select>
    </div>
</div>
<div class="input-group search-box">
    <input type="text" class="form-control" placeholder="Search..."
            ng-model="search_input.$" ng-change="vacationDaysCal()">
</div>
