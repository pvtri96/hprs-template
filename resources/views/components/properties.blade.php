<th>ID</th>
<th>Name</th>
<th ng-if="isShowBirthday">Birthday</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Shareholder
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="status in shareholderStatus" ng-click="filterBy('shareholder_status', status.code)">
                <a ng-bind="status.name"></a>
            </li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Gender
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="gender in genders" ng-click="filterBy('gender', gender.code)">
                <a ng-bind="gender.name"></a>
            </li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Ethnicity
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="ethnicity in ethnicities" ng-click="filterBy('ethnicity', ethnicity)">
                <a ng-bind="ethnicity"></a>
            </li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Job Type
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="jobType in jobTypes" ng-click="filterBy('pay_type', jobType.code)">
                <a ng-bind="jobType.name"></a>
            </li>
        </ul>
    </div>
</th>
<th>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Department
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li ng-click="resetFilter()">
                <a>All</a>
            </li>
            <li ng-repeat="department in departments" ng-click="filterBy('department', department)">
                <a ng-bind="department"></a>
            </li>
        </ul>
    </div>
</th>
<th>Position</th>
