<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <li class="nav-item" ui-sref="dashboard" ui-sref-active="active">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <span class="title">Home</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Management</h3>
            </li>
            <li class="nav-item" ng-repeat="permission in Auth.permissions"
                ng-if="permission.feature_id == 1 || permission.feature_id == 2"
                ui-sref="[[permission.state_name]].list" ui-sref-active="active">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    <span class="title" ng-bind="permission.feature_name"></span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Features</h3>
            </li>
            <li class="nav-item" ng-repeat="permission in Auth.permissions"
                ng-if="permission.feature_id != 1 && permission.feature_id != 2 && permission.feature_id != 3"
                ui-sref="[[permission.state_name]].list" ui-sref-active="active">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    <span class="title" ng-bind="permission.feature_name"></span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Actions</h3>
            </li>
            <li class="nav-item" ng-repeat="permission in Auth.permissions"
                ng-if="permission.feature_id == 3"
                ui-sref="[[permission.state_name]].list" ui-sref-active="active">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    <span class="title" ng-bind="permission.feature_name"></span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
