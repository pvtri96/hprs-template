<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a href="{{ route('view.dashboard') }}">
                <span class="title logo">HPRS</span>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-danger"> 2 </span>
                    </a>
                    <ul class="dropdown-menu">
                        <div class="dropdown-menu-title">
                            <span class="bold-font notification-title">Notifications</span>
                        </div>
                        <hr>
                        <div class="dropdown-menu-content">
                            <div class="content birthday">
                                <i class="fa fa-birthday-cake dropdown-menu-content-icon" aria-hidden="true"></i>
                                <span>It's <span class="bold-font">Van Tri Pham's</span> birthday today. Wish him well!</span>
                            </div>
                            <hr>
                            <div class="content hiring-aniversary">
                                <i class="fa fa-handshake-o dropdown-menu-content-icon" aria-hidden="true"></i>
                                <span>Today is <span class="bold-font">Quoc Manh Le's</span> 5 working years in company. Congratulate him!</span>
                            </div>
                        </div>
                    </ul>
                </li>
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="../assets/image/avatar3_small.png" />
                        <span class="username username-hide-on-mobile" ng-cloak>
                            <div ng-if="Auth.user.first_name || Auth.user.last_name">[[ Auth.user.first_name ]] [[ Auth.user.last_name ]]</div>
                            <div ng-if="!Auth.user.first_name && !Auth.user.last_name">[[ Auth.user.email ]]</div>
                        </span>
                    </a>
                </li>
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="{{ route('view.auth.logout') }}" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
