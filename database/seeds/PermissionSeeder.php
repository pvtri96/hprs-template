<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'user_id' => '1',
                'authority_id' => '1',
            ],
            [
                'user_id' => '2',
                'authority_id' => '2',
            ],
            [
                'user_id' => '3',
                'authority_id' => '3',
            ],
            [
                'user_id' => '4',
                'authority_id' => '4',
            ],
        ];

        foreach ($permissions as $permission) {
            switch ($permission['authority_id']) {
                case 1:
                    $features = [1,2,3,4,5,6,7];
                    break;
                case 2:
                    $features = [2,3,4,5,6,7];
                    break;
                case 3:
                    $features = [4,5,6,7];
                    break;
                case 4:
                    $features = [5,7];
                    break;
                case 5:
                    $features = [4,6];
                    break;
                default:
                    $features = [];
                    break;
            }
            foreach ($features as $feature) {
                $permission['feature_id'] = $feature;
                App\Model\Permission::create($permission);    
            }
        };
    }
}
