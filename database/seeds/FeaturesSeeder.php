<?php

use Illuminate\Database\Seeder;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $features = [
            [ 'name' => 'Users management' ],
            [ 'name' => 'Employees management' ],
            [ 'name' => 'Set alerts' ],
        	[ 'name' => 'Total earnings' ],
        	[ 'name' => 'Number of vacation days' ],
        	[ 'name' => 'Average benefits' ],
        	[ 'name' => 'Birthday in month' ]
        ];

		foreach($features as $feature) {
			App\Model\Feature::create([
	        	'name' => $feature['name']
	        ]);
		}
    }
}
