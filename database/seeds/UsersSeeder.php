<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => 'System',
                'last_name' => 'Admin',
                'department' => 'Human Resource Manager',
                'email' => 'admin@admin.com',
                'password' => 'admin'
            ],

            [
                'first_name' => 'Pham',
                'last_name' => 'Tri',
                'department' => 'CEO',
                'email' => 'pvtri96@gmail.com',
                'password' => '123456'
            ],

            [
                'first_name' => 'Le',
                'last_name' => 'Dung',
                'department' => 'Management Team',
                'email' => 'dungle1811@gmail.com',
                'password' => 'thuydung'
            ],

            [
                'first_name' => 'Le',
                'last_name' => 'Manh',
                'department' => 'Quet rac Team',
                'email' => 'manhle@gmail.com',
                'password' => 'quocmanh'
            ]
        ];

        foreach ($users as $user) {
            App\Model\User::create([
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'department' => $user['department'],
                'email' => $user['email'],
                'password' => bcrypt($user['password'])
            ]);  
        };
    }
}
