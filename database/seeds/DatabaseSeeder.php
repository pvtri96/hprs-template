<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersSeeder::class);
         $this->call(AuthoritiesSeeder::class);
         $this->call(FeaturesSeeder::class);
         $this->call(PermissionSeeder::class);

         //Data for HumanResource
         $this->call(HumanResourceSeeder::class);
         //Data for Payroll
         $this->call(PayrollSeeder::class);
    }
}
