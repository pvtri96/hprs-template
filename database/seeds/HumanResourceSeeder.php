<?php

use Illuminate\Database\Seeder;

use App\Model\HRPersonal;
use App\Model\Employment;
use App\Model\JobHistory;
use App\Model\BenefitPlan;

class HumanResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employment::truncate();

    	JobHistory::truncate();

    	HRPersonal::truncate();

    	BenefitPlan::truncate();

        //Dumped data for BenefitPlan
        $benefit_plans = static::getBenefitPlans();

        foreach ($benefit_plans as $benefit_plan) {
        	BenefitPlan::insert($benefit_plan);
        }

    	//Dumped data for Personal
        $hr = static::getPersonals();

        foreach ($hr as $person) {
	        HRPersonal::insert($person);
        }

        //Dumped data for JobHistory
        $job_histories = static::getJobHistories();

        foreach ($job_histories as $job_history) {
        	JobHistory::insert($job_history);
        }

        //Dumped data for Employment
        $employments = static::getEmployments();

        foreach ($employments as $employment) {
        	Employment::insert($employment);
        }
    }

    public static function getPersonals()
    {
    	return array (
		  0 => 
		  array(
		     'Employee_ID' => '1',
		     'First_Name' => 'Tri',
		     'Last_Name' => 'Pham',
		     'Middle_Initial' => 'Van',
		     'Address1' => '24 Tang Bat Ho',
		     'Address2' => NULL,
		     'City' => 'Da Nang',
		     'State' => 'Viet Nam',
		     'Zip' => '55000',
		     'Email' => 'pvtri96@gmail.com',
		     'Phone_Number' => '01659331448',
		     'Social_Security_Number' => '2021124634',
		     'Drivers_License' => 'DRIVER09182323',
		     'Marital_Status' => '1',
		     'Gender' => '1',
		     'Shareholder_Status' => '1',
		     'Benefit_Plans' => NULL,
		     'Ethnicity' => 'Kinh',
		  ),
		  1 => 
		  array(
		     'Employee_ID' => '2',
		     'First_Name' => 'Dung',
		     'Last_Name' => 'Le',
		     'Middle_Initial' => 'Thi Thuy',
		     'Address1' => '344 Ngu Hanh Son',
		     'Address2' => NULL,
		     'City' => 'Da Nang',
		     'State' => 'Viet Nam',
		     'Zip' => '55000',
		     'Email' => 'ledung1811@gmail.com',
		     'Phone_Number' => '01239837454',
		     'Social_Security_Number' => '2021124348',
		     'Drivers_License' => 'DRIVER92034823',
		     'Marital_Status' => '1',
		     'Gender' => '0',
		     'Shareholder_Status' => '1',
		     'Benefit_Plans' => NULL,
		     'Ethnicity' => 'Muong',
		  ),
		  2 => 
		  array(
		     'Employee_ID' => '3',
		     'First_Name' => 'Manh',
		     'Last_Name' => 'Le',
		     'Middle_Initial' => 'Quoc',
		     'Address1' => '164 Hoang Dieu',
		     'Address2' => NULL,
		     'City' => 'Da Nang',
		     'State' => 'Viet Nam',
		     'Zip' => '55000',
		     'Email' => 'manhle@gmail.com',
		     'Phone_Number' => '01262201096',
		     'Social_Security_Number' => '2021129394',
		     'Drivers_License' => 'DRIVER83243888',
		     'Marital_Status' => '1',
		     'Gender' => '1',
		     'Shareholder_Status' => '1',
		     'Benefit_Plans' => NULL,
		     'Ethnicity' => 'Kinh',
		  ),
		  3 => 
		  array(
		     'Employee_ID' => '4',
		     'First_Name' => 'Dung',
		     'Last_Name' => 'Duong',
		     'Middle_Initial' => 'Cong',
		     'Address1' => '12 Hoang Hoa Tham',
		     'Address2' => NULL,
		     'City' => 'Da Nang',
		     'State' => 'Viet Nam',
		     'Zip' => '55000',
		     'Email' => 'finecuden@gmail.com',
		     'Phone_Number' => '02384763888',
		     'Social_Security_Number' => '2021129812',
		     'Drivers_License' => 'DRIVER02813342',
		     'Marital_Status' => '1',
		     'Gender' => '0',
		     'Shareholder_Status' => '0',
		     'Benefit_Plans' => NULL,
		     'Ethnicity' => 'HMong',
		  ),
		  4 => 
		  array(
		     'Employee_ID' => '5',
		     'First_Name' => 'Vinh',
		     'Last_Name' => 'Lu',
		     'Middle_Initial' => 'Thanh',
		     'Address1' => '43 Tran Hung Dao',
		     'Address2' => NULL,
		     'City' => 'Da Nang',
		     'State' => 'Viet Nam',
		     'Zip' => '55000',
		     'Email' => 'vinhlu@gmail.com',
		     'Phone_Number' => '0934783743',
		     'Social_Security_Number' => '2021121230',
		     'Drivers_License' => 'DRIVER09345834',
		     'Marital_Status' => '1',
		     'Gender' => '1',
		     'Shareholder_Status' => '0',
		     'Benefit_Plans' => NULL,
		     'Ethnicity' => 'Tay',
		  ),
		  5 => 
		  array(
		     'Employee_ID' => '6',
		     'First_Name' => 'Nguyen',
		     'Last_Name' => 'Tran',
		     'Middle_Initial' => 'Hoang Phuoc',
		     'Address1' => '230 Le Thanh Nghi',
		     'Address2' => NULL,
		     'City' => 'Da Nang',
		     'State' => 'Viet Nam',
		     'Zip' => '55000',
		     'Email' => 'anglejudgement@gmail.com',
		     'Phone_Number' => '01238726323',
		     'Social_Security_Number' => '2021129865',
		     'Drivers_License' => 'DRIVER98237433',
		     'Marital_Status' => '1',
		     'Gender' => '0',
		     'Shareholder_Status' => '0',
		     'Benefit_Plans' => NULL,
		     'Ethnicity' => 'HMong',
		  ),
		);
    }

    public static function getJobHistories()
    {
    	return array (
		  0 => 
		  array(
		     'Employee_ID' => '1',
		     'Department' => 'Operator',
		     'Division' => 'CEO',
		     'Start_Date' => '2016-02-14 00:00:00.000',
		     'End_Date' => NULL,
		     'Job_Title' => 'CEO',
		     'Supervisor' => NULL,
		     'Job_Category' => NULL,
		     'Location' => 'Da Nang',
		     'Departmen_Code' => '001',
		     'Salary_Type' => NULL,
		     'Pay_Period' => 'Monthly',
		     'Hours_per_Week' => '64',
		     'Hazardous_Training' => '1',
		  ),
		  1 => 
		  array(
		     'Employee_ID' => '2',
		     'Department' => 'Team',
		     'Division' => 'Manager',
		     'Start_Date' => '2016-02-14 00:00:00.000',
		     'End_Date' => NULL,
		     'Job_Title' => 'Manager',
		     'Supervisor' => NULL,
		     'Job_Category' => NULL,
		     'Location' => 'Da Nang',
		     'Departmen_Code' => '001',
		     'Salary_Type' => NULL,
		     'Pay_Period' => 'Monthly',
		     'Hours_per_Week' => '64',
		     'Hazardous_Training' => '1',
		  ),
		  2 => 
		  array(
		     'Employee_ID' => '3',
		     'Department' => 'Quet rac',
		     'Division' => 'Manager',
		     'Start_Date' => '2016-02-14 00:00:00.000',
		     'End_Date' => NULL,
		     'Job_Title' => 'Manager',
		     'Supervisor' => NULL,
		     'Job_Category' => NULL,
		     'Location' => 'Da Nang',
		     'Departmen_Code' => '001',
		     'Salary_Type' => NULL,
		     'Pay_Period' => 'Monthly',
		     'Hours_per_Week' => '64',
		     'Hazardous_Training' => '1',
		  ),
		  3 => 
		  array(
		     'Employee_ID' => '4',
		     'Department' => 'Sercurity',
		     'Division' => 'Employee',
		     'Start_Date' => '2016-02-14 00:00:00.000',
		     'End_Date' => NULL,
		     'Job_Title' => 'Employee',
		     'Supervisor' => NULL,
		     'Job_Category' => NULL,
		     'Location' => 'Da Nang',
		     'Departmen_Code' => '001',
		     'Salary_Type' => NULL,
		     'Pay_Period' => 'Weekly',
		     'Hours_per_Week' => '36',
		     'Hazardous_Training' => '1',
		  ),
		  4 => 
		  array(
		     'Employee_ID' => '5',
		     'Department' => 'Sercurity',
		     'Division' => 'Employee',
		     'Start_Date' => '2016-02-14 00:00:00.000',
		     'End_Date' => NULL,
		     'Job_Title' => 'Employee',
		     'Supervisor' => NULL,
		     'Job_Category' => NULL,
		     'Location' => 'Da Nang',
		     'Departmen_Code' => '001',
		     'Salary_Type' => NULL,
		     'Pay_Period' => 'Weekly',
		     'Hours_per_Week' => '36',
		     'Hazardous_Training' => '1',
		  ),
		  5 => 
		  array(
		     'Employee_ID' => '6',
		     'Department' => 'Sercurity',
		     'Division' => 'Employee',
		     'Start_Date' => '2016-02-14 00:00:00.000',
		     'End_Date' => NULL,
		     'Job_Title' => 'Employee',
		     'Supervisor' => NULL,
		     'Job_Category' => NULL,
		     'Location' => 'Da Nang',
		     'Departmen_Code' => '001',
		     'Salary_Type' => NULL,
		     'Pay_Period' => 'Weekly',
		     'Hours_per_Week' => '36',
		     'Hazardous_Training' => '1',
		  ),
		);
    }

    public static function getBenefitPlans()
    {
    	return array (
		  0 => 
		  array(
		     'Plan_Name' => 'Vacation Plan',
		     'Deductable' => '1',
		     'Percentage_CoPay' => '15',
		  ),
		  1 => 
		  array(
		     'Plan_Name' => 'Tet Holiday',
		     'Deductable' => '2',
		     'Percentage_CoPay' => '25',
		  ),
		);
    }

    public static function getEmployments()
    {
    	return array (
		  0 => 
		  array(
		     'Employee_ID' => '1',
		     'Employment_Status' => '1',
		     'Hire_Date' => '2016-02-14 00:00:00.000',
		     'Workers_Comp_Code' => '20128',
		     'Termination_Date' => '2017-01-14 00:00:00.000',
		     'Rehire_Date' => NULL,
		     'Last_Review_Date' => '2016-07-18 00:00:00.000',
		  ),
		  1 => 
		  array(
		     'Employee_ID' => '2',
		     'Employment_Status' => '1',
		     'Hire_Date' => '2016-03-14 00:00:00.000',
		     'Workers_Comp_Code' => '20938',
		     'Termination_Date' => '2017-01-14 00:00:00.000',
		     'Rehire_Date' => NULL,
		     'Last_Review_Date' => '2016-07-18 00:00:00.000',
		  ),
		  2 => 
		  array(
		     'Employee_ID' => '3',
		     'Employment_Status' => '1',
		     'Hire_Date' => '2016-04-14 00:00:00.000',
		     'Workers_Comp_Code' => '20939',
		     'Termination_Date' => '2017-01-14 00:00:00.000',
		     'Rehire_Date' => NULL,
		     'Last_Review_Date' => '2016-07-18 00:00:00.000',
		  ),
		  3 => 
		  array(
		     'Employee_ID' => '4',
		     'Employment_Status' => '1',
		     'Hire_Date' => '2016-05-14 00:00:00.000',
		     'Workers_Comp_Code' => '20939',
		     'Termination_Date' => '2017-01-14 00:00:00.000',
		     'Rehire_Date' => NULL,
		     'Last_Review_Date' => '2016-07-18 00:00:00.000',
		  ),
		  4 => 
		  array(
		     'Employee_ID' => '5',
		     'Employment_Status' => '1',
		     'Hire_Date' => '2016-06-14 00:00:00.000',
		     'Workers_Comp_Code' => '21237',
		     'Termination_Date' => '2017-01-14 00:00:00.000',
		     'Rehire_Date' => NULL,
		     'Last_Review_Date' => '2016-07-18 00:00:00.000',
		  ),
		  5 => 
		  array(
		     'Employee_ID' => '6',
		     'Employment_Status' => '1',
		     'Hire_Date' => '2016-07-14 00:00:00.000',
		     'Workers_Comp_Code' => '21237',
		     'Termination_Date' => '2017-01-14 00:00:00.000',
		     'Rehire_Date' => NULL,
		     'Last_Review_Date' => '2016-07-18 00:00:00.000',
		  ),
		);
    }
}
