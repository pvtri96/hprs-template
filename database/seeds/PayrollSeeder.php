<?php

use Illuminate\Database\Seeder;

use App\Model\PREmployee;
use App\Model\PayRate;

class PayrollSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::connection('prsql')->unprepared('
			DELETE FROM `employee`;
			ALTER TABLE `employee` AUTO_INCREMENT = 1;
			DELETE FROM `pay rates`;
			ALTER TABLE `pay rates` AUTO_INCREMENT = 1;
    	');
    	PayRate::insert(static::getPayRate());
    	PREmployee::insert(static::getEmployees());
    }

    private static function getEmployees()
    {
    	return array (
		  0 => 
		  array(
		     'idEmployee' => 1,
		     'Employee Number' => 1,
		     'Last Name' => 'Pham',
		     'First Name' => 'Tri',
		     'SSN' => '2021124634',
		     'Pay Rate' => '13',
		     'Pay Rates_idPay Rates' => 1,
		     'Vacation Days' => 30,
		     'Paid To Date' => '20',
		     'Paid Last Year' => '80',
		  ),
		  1 => 
		  array(
		     'idEmployee' => 2,
		     'Employee Number' => 2,
		     'Last Name' => 'Le',
		     'First Name' => 'Dung',
		     'SSN' => '2021124348',
		     'Pay Rate' => '14',
		     'Pay Rates_idPay Rates' => 3,
		     'Vacation Days' => 25,
		     'Paid To Date' => '12',
		     'Paid Last Year' => '65',
		  ),
		  2 => 
		  array(
		     'idEmployee' => 3,
		     'Employee Number' => 3,
		     'Last Name' => 'Le',
		     'First Name' => 'Manh',
		     'SSN' => '2021129394',
		     'Pay Rate' => '10',
		     'Pay Rates_idPay Rates' => 3,
		     'Vacation Days' => 7,
		     'Paid To Date' => '13',
		     'Paid Last Year' => '14',
		  ),
		  3 => 
		  array(
		     'idEmployee' => 4,
		     'Employee Number' => 4,
		     'Last Name' => 'Cong',
		     'First Name' => 'Dung',
		     'SSN' => '2021129812',
		     'Pay Rate' => '2',
		     'Pay Rates_idPay Rates' => 3,
		     'Vacation Days' => 12,
		     'Paid To Date' => '1',
		     'Paid Last Year' => '50',
		  ),
		  4 => 
		  array(
		     'idEmployee' => 5,
		     'Employee Number' => 5,
		     'Last Name' => 'Lu',
		     'First Name' => 'Vinh',
		     'SSN' => '2021121230',
		     'Pay Rate' => '8',
		     'Pay Rates_idPay Rates' => 4,
		     'Vacation Days' => 12,
		     'Paid To Date' => '45',
		     'Paid Last Year' => '10',
		  ),
		  5 => 
		  array(
		     'idEmployee' => 6,
		     'Employee Number' => 6,
		     'Last Name' => 'Tran',
		     'First Name' => 'Nguyen',
		     'SSN' => '2021129865',
		     'Pay Rate' => '4',
		     'Pay Rates_idPay Rates' => 3,
		     'Vacation Days' => 32,
		     'Paid To Date' => '21',
		     'Paid Last Year' => '65',
		  ),
		);
    }

    private static function getPayRate()
    {
    	return array (
		  0 => 
		  array(
		     'idPay Rates' => 1,
		     'Pay Rate Name' => 'Senior',
		     'Value' => '154',
		     'Tax Percentage' => '6',
		     'Pay Type' => 1,
		     'Pay Amount' => '4500000',
		     'PT - Level C' => '1',
		  ),
		  1 => 
		  array(
		     'idPay Rates' => 2,
		     'Pay Rate Name' => 'Guru',
		     'Value' => '186',
		     'Tax Percentage' => '8',
		     'Pay Type' => 1,
		     'Pay Amount' => '5500000',
		     'PT - Level C' => '1',
		  ),
		  2 => 
		  array(
		     'idPay Rates' => 3,
		     'Pay Rate Name' => 'Junior',
		     'Value' => '121',
		     'Tax Percentage' => '5',
		     'Pay Type' => 1,
		     'Pay Amount' => '3500000',
		     'PT - Level C' => '1',
		  ),
		  3 => 
		  array(
		     'idPay Rates' => 4,
		     'Pay Rate Name' => 'Newbie',
		     'Value' => '108',
		     'Tax Percentage' => '2',
		     'Pay Type' => 2,
		     'Pay Amount' => '1500000',
		     'PT - Level C' => '1',
		  ),
		);
    }
}
