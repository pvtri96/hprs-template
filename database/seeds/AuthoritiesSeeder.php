<?php

use Illuminate\Database\Seeder;

class AuthoritiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authorities = [
            [ 'name' => 'Administrator' ],
            [ 'name' => 'CEO' ],
            [ 'name' => 'Senior Manager' ],
            [ 'name' => 'Team Manager' ],
            [ 'name' => 'Shareholder' ]
        ];

        foreach($authorities as $authority) {
            App\Model\Authority::create([
                'name' => $authority['name']
            ]);   
        }
    }
}
