<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER after_update_user_permission AFTER UPDATE ON users FOR EACH ROW
            BEGIN
                UPDATE permissions
                    SET user_email = NEW.email 
                    WHERE user_id = NEW.user_id;
            END
        ');

        DB::unprepared('
            CREATE TRIGGER after_update_authority_permission AFTER UPDATE ON authorities FOR EACH ROW
            BEGIN
                UPDATE permissions
                    SET authority_name = NEW.name 
                    WHERE authority_id = NEW.authority_id;
            END
        ');

        DB::unprepared('
            CREATE TRIGGER after_update_feature_permission AFTER UPDATE ON features FOR EACH ROW
            BEGIN
                UPDATE permissions
                    SET feature_name = NEW.name
                    WHERE feature_id = NEW.feature_id;
            END
        ');

        $insert_permission_procedure = 
            'BEGIN
                SET NEW.user_email = (SELECT email FROM users WHERE user_id = NEW.user_id);
                SET NEW.authority_name = (SELECT name FROM authorities WHERE authority_id = NEW.authority_id);
                SET NEW.feature_name = (SELECT name FROM features WHERE feature_id = NEW.feature_id);
            END';

        DB::unprepared('
            CREATE TRIGGER before_insert_permission BEFORE INSERT ON permissions FOR EACH ROW '
                . $insert_permission_procedure
        );

        DB::unprepared('
            CREATE TRIGGER before_update_permission BEFORE UPDATE ON permissions FOR EACH ROW '
                . $insert_permission_procedure
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER after_update_user_permission');
        DB::unprepared('DROP TRIGGER after_update_authority_permission');
        DB::unprepared('DROP TRIGGER after_update_feature_permission');
        DB::unprepared('DROP TRIGGER before_insert_permission');
        DB::unprepared('DROP TRIGGER before_update_permission');
    }
}
