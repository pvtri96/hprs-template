<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('permission_id');
            $table->integer('user_id')->unsigned();
            $table->string('user_email', 100)->nullable();
            $table->integer('authority_id')->unsigned();
            $table->string('authority_name', 100)->nullable();
            $table->integer('feature_id')->unsigned();
            $table->string('feature_name', 100)->nullable();
            $table->integer('status')->unsigned()->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
