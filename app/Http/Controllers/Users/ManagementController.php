<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;

class ManagementController extends Controller
{
    public function getAllUsers()
    {
    	
    	$users = User::select(['user_id', 'first_name', 'last_name', 'email', 'department', 'created_at'])->get();

    	return response()->json([
    		'message' => 'Get all users successful',
    		'success' => true,
    		'data' => $users
    	]);
    }
}
