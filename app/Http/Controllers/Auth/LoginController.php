<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Auth;
use App\Model\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Auth guard
     */
    protected $auth;

    /**
     * lockoutTime
     */
    protected $lockoutTime;

    /**
     * maxLoginAttempts
     */
    protected $maxLoginAttempts;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct(Guard $auth)
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->auth = $auth;
        $this->lockoutTime = 1;
        $this->maxLoginAttempts = 5;
    }

    protected function hasTooManyLoginAttempts(LoginRequest $request) {
        return $this->limiter()->tooManyAttempts($this->throttleKey($request), $this->maxLoginAttempts, $this->lockoutTime);
    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        if ($this->auth->attempt($user)) {
            // clear login session
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            $this->generateAPIToken();

            return redirect()->route('view.dashboard');
        } else {
            $this->incrementLoginAttempts($request);

            Session::flash('message', trans('auth.validate_account'));
            return redirect()->back()->withInput();
        }
    }

    public function logout()
    {
        $this->guard()->logout();

        // Removing All Items From The Session
        Session::flush();
        Session::regenerate();

        Session::flash('message', trans('auth.logout_success'));

        return redirect()->route('view.auth.login')->withInput();
    }

    private function generateAPIToken()
    {
        $user = Auth::user();
        $user['api_token'] = bcrypt($user['email'] . time());
        $user->save();
    }
}
