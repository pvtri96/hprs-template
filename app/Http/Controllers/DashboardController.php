<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\Permission;

class DashboardController extends Controller
{
    public function getCurrentUser()
    {
    	$auth = Auth::guard('api')->user()->getAttributes();
    	$auth = collect($auth)->except(['api_token', 'remember_token', 'password', 'created_at', 'updated_at']);

    	return response()->json([
    		'message' => 'Get current user successful.',
    		'success' => true,
    		'data' => $auth
    	]);
    }

    public static function getPermissions($user_id)
    {
        $permissions = Permission::where('user_id', $user_id)->get();
        return $permissions;
    }
}
