<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\HRPersonal;
use App\Model\PREmployee;
use App\Model\PayRate;
use App\Model\JobHistory;

class TotalEarningController extends Controller
{
    public function getTotalEarnings()
    {
    	$employees = Employee::withPayRate()->withJobHistories()->get();
    	return response()->json([
    		'message' => 'Get employees successful',
    		'success' => true,
    		'data' => $employees
    	]);
    }

    public function getTotalEarningDetail($employee_SSN)
    {
    	$employee_HR = HRPersonal::where('Social_Security_Number', $employee_SSN)->first();
    	$employee_PR = PREmployee::where('SSN', $employee_SSN)->first();

    	$employee = Employee::normalize($employee_HR, $employee_PR);

        $pay_rate = PayRate::where('idPay Rates', $employee['pay_rates_id_pay_rates'])->first();
        $job_histories = JobHistory::where('Employee_ID', $employee['employee_id'])->orderBy('ID', 'desc')->get();

        $employee['job_histories'] = array();
        array_push($employee['job_histories'], [
            'department' => $job_histories[0]['Department'],
            'division' => $job_histories[0]['Division'],
            'hours_per_week' => $job_histories[0]['Hours_per_Week']
        ]);

        $employee['pay_rate'] = [
            'pay_type' => $pay_rate['Pay Type'],
            'pay_type_level' => $pay_rate['PT - Level C'],
            'value' => $pay_rate['Value'],
            'pay_amount' => $pay_rate['Pay Amount'],
            'tax_percentage' => $pay_rate['Tax Percentage']
        ];

		return response()->json([
    		'message' => 'Get employee successful',
    		'success' => true,
    		'data' => $employee
    	]);
    }
}
