<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\JobHistory;
use App\Model\PayRate;

class VacationDaysController extends Controller
{
    public function getVacationDays()
    {
    	$employees = Employee::withPayRate()->withJobHistories()->get();

    	return response()->json([
    		'message' => 'Get employees successful',
    		'success' => true,
    		'data' => $employees
    	]);
    }

    public function getVacationDayDetail($SSN)
    {
        $employee = Employee::find($SSN);
        $job_histories = JobHistory::where('Employee_ID', $employee['employee_id'])->orderBy('ID', 'desc')->get();
        $pay_rate = PayRate::where('idPay Rates', $employee['pay_rates_id_pay_rates'])->first();

        $employee['job_histories'] = [
            'department' => $job_histories[0]['Department'],
            'division' => $job_histories[0]['Division']
        ];

        $employee['pay_rate'] = [
            'pay_type' => $pay_rate['Pay Type'],
        ];

    	return response()->json([
    		'message' => 'Get employee successful',
    		'success' => true,
    		'data' => $employee
    	]);
    }
}
