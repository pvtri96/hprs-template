<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\HRPersonal;
use App\Model\Employment;
use App\Model\PREmployee;
use App\Model\JobHistory;
use App\Model\Employee;
use App\Model\PayRate;

class EmployeeController extends Controller
{
    public function getAllEmployees()
    {
        $employees = Employee::withJobHistories()
                    ->withEmployments()
                    ->withBenefitPlan()
                    ->withPayRate()
                    ->withEmployments()
                    ->get();

    	return response()->json([
    		'message' => 'Get employees successful',
    		'success' => true,
    		'data' => $employees
    	]);
    }

    public function getEmployeeDetail($employee_SSN)
    {
    	$employee = HRPersonal::where('Social_Security_Number', $employee_SSN)->first();
        $job_histories = JobHistory::where('Employee_ID', $employee['Employee_ID'])->orderBy('ID', 'desc')->get();
        $pay_rate = PayRate::where('idPay Rates', $employee['pay_rates_id_pay_rates'])->first();

        $employee['job_histories'] = [
            'department' => $job_histories[0]['Department'],
            'division' => $job_histories[0]['Division'],
            'location' => $job_histories[0]['Location']
        ];

        $employee['pay_rate'] = [
            'pay_type' => $pay_rate['Pay Type'],
        ];

    	return response()->json([
    		'message' => 'Get employee successful',
    		'success' => true,
    		'data' => static::normalizeEmployeeWithJobHistories($employee)
    	]);	
    }

    public function delete(Request $request)
    {
        $employee = $request->all();

        $deleted_employee = PREmployee::where('SSN', $employee['SSN'])->delete();
        $deleted_employment = Employment::where('Employee_ID', $employee['employee_id'])->delete();
        $deleted_personal = HRPersonal::where('Social_Security_Number', $employee['SSN'])->delete();

        return response()->json([
            'message' => 'Delete successful',
            'success' => true,
            'data' => $deleted_personal
        ]);
    }

    public function create(Request $request)
    {
        
    }

    private static function normalizeEmployeeWithJobHistories($employee)
    {
        $normalized_employee = HRPersonal::normalize($employee);
        $normalized_employee['job_histories'] = $employee['job_histories'];

        return $normalized_employee;
    }
}
