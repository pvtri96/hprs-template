<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;
use Illuminate\Contracts\Auth\Guard;

class Authenticate
{
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->ajax() || $request->wantsJson()) {
            if (Auth::guard('api')->user())
                return $next($request);
            else 
                return response('Unauthorized.', 401);
        }
        if ($this->auth->guest()) {
            Session::flash('message', trans('auth.required'));
            return redirect()->route('view.auth.login');
        }
        return $next($request);
    }
}
