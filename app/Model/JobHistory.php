<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JobHistory extends Model
{
    protected $connection = 'hrsql';
    protected $table = 'Job_History';

    protected $fillable = ['ID', 'Employee_ID', 'Department', 'Division', 'Start_Date', 'End_Date', 'Job_Title', 'Supervisor', 'Job_Category', 'Location', 'Departmen_Code', 'Salary_Type', 'Pay_Period', 'Hours_per_Week', 'Hazardous_Training'];

    public function person()
    {
    	return $this->belongsTo('App\Model\HRPersonal', 'Employee_ID', 'Employee_ID');
    }

    public static function normalize($employement)
    {
    	return [
    		'id' => $employement['ID'],
    		'employee_id' => $employement['Employee_ID'],
    		'department' => $employement['Department'],
    		'division' => $employement['Division'],
    		'start_date' => $employement['Start_Date'],
    		'end_date' => $employement['End_Date'],
    		'job_title' => $employement['Job_Title'],
    		'supervisor' => $employement['Supervisor'],
    		'job_category' => $employement['Job_Category'],
    		'location' => $employement['Location'],
    		'departmen_code' => $employement['Departmen_Code'],
    		'salary_type' => $employement['Salary_Type'],
    		'pay_period' => $employement['Pay_Period'],
    		'hours_per_week' => $employement['Hours_per_Week'],
    		'hazardous_training' => $employement['Hazardous_Training'],
    	];
    }
}
