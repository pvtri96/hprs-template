<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PREmployee extends Model
{
    protected $connection = 'prsql';
    protected $table = 'employee';

    protected $fillable = ['idEmployee', 'Employee Number', 'Last Name', 'First Name', 'SSN', 'Pay Rate', 'Pay Rates_idPay Rates', 'Vacation Days', 'Paid To Date', 'Paid Last Year'];

    public function pay_rate()
    {
    	return $this->belongsTo('App\Model\PayRate', 'Pay Rates_idPay Rates', 'idPay Rates');
    }

    public static function normalize($employee)
    {
    	return [
    		'employee_id' => $employee['idEmployee'],
    		'employee_number' => $employee['Employee Number'],
    		'first_name' => $employee['First Name'],
    		'last_name' => $employee['Last Name'],
    		'SSN' => $employee['SSN'],
    		'pay_rate' => $employee['Pay Rate'],
    		'pay_rates_id_pay_rates' => $employee['Pay Rates_idPay Rates'],
    		'vacation_days' => $employee['Vacation Days'],
    		'paid_to_date' => $employee['Paid To Date'],
    		'paid_last_year' => $employee['Paid Last Year'],
    	];
    }
}
