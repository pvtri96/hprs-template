<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class BenefitPlan extends Model
{
    protected $connection = 'hrsql';
    protected $table = 'Benefit_Plans';

    protected $fillable = ['Benefit_Plan_ID', 'Plan_Name', 'Deductable', 'Percentage_CoPay'];

    public function person()
    {
    	return $this->hasMany('App\Model\HRPersonal', 'Benefit_Plan_ID', 'Benefit_Plans');
    }

    public static function normalize($employement)
    {
    	return [
    		'benefit_plan_id' => $employement['Benefit_Plan_ID'],
    		'plan_name' => $employement['Plan_Name'],
    		'deductable' => $employement['Deductable'],
    		'percentage_copay' => $employement['Percentage_CoPay'],
    	];
    }

    public static function truncate()
    {
        DB::connection('hrsql')->statement('
            DELETE from Benefit_Plans
            DBCC CHECKIDENT (\'Benefit_Plan_ID\' , RESEED, 0)
        ');
    }
}
