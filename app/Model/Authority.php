<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
    protected $primaryKey = 'authority_id';

    protected $fillable = ['name'];
}
