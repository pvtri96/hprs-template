<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PayRate extends Model
{
    protected $connection = 'prsql';
    protected $table = 'pay rates';

    protected $fillable = ['idPay Rates', 'Pay Rate Name', 'Value', 'Tax Percentage', 'Pay Type', 'Pay Amount', 'PT - Level C'];

    public function employee()
    {
    	return $this->belongsTo('App\Model\PREmployee', 'idPay Rates', 'Pay Rates_idPay RatesPrimaryIndex');
    }

    public static function normalize($employement)
    {
    	return [
    		'pay_rate_id' => $employement['idPay Rates'],
    		'pay_rate_name' => $employement['Pay Rate Name'],
    		'value' => $employement['Value'],
    		'tax_percentage' => $employement['Tax Percentage'],
    		'pay_type' => $employement['Pay Type'],
    		'pay_amount' => $employement['Pay Amount'],
    		'pt_level_C' => $employement['PT - Level C'],
    	];
    }
}
