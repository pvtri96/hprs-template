<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class HRPersonal extends Model
{
    protected $connection = 'hrsql';
    protected $table = 'Personal';

    protected $fillable = ['Employee_ID', 'Last_Name', 'First_Name', 'Middle_Initial', 'Day_of_Birth', 'Address1', 'Address2', 'City', 'State', 'Zip', 'Email', 'Phone_Number', 'Social_Security_Number', 'Drivers_License', 'Marital_Status', 'Gender', 'Shareholder_Status', 'Benefit_Plans', 'Ethnicity'];

    public function employment()
    {
    	return $this->hasOne('App\Model\Employment', 'Employee_ID', 'Employee_ID');
    }

    public function benefit_plan()
    {
    	return $this->belongsTo('App\Model\BenefitPlan', 'Benefit_Plan_ID', 'Benefit_Plans');
    }
    
    public function job_histories()
    {
    	return $this->hasMany('App\Model\JobHistory', 'Employee_ID', 'Employee_ID');
    }

    public static function normalize($person)
    {
    	return [
    		'employee_id' => $person['Employee_ID'],
    		'SSN' => $person['Social_Security_Number'],
    		'first_name' => $person['First_Name'],
    		'last_name' => $person['Last_Name'],
    		'middle_name' => $person['Middle_Initial'],
    		'day_of_birth' => $person['Day_of_Birth'],
    		'address1' => $person['Address1'],
    		'address2' => $person['Address2'],
    		'city' => $person['City'],
    		'state' => $person['State'],
    		'zip' => $person['Zip'],
    		'email' => $person['Email'],
    		'phone_number' => $person['Phone_Number'],
    		'drivers_license' => $person['Drivers_License'],
    		'marital_status' => $person['Marital_Status'],
    		'gender' => $person['Gender'],
    		'shareholder_status' => $person['Shareholder_Status'],
    		'benefit_plans' => $person['Benefit_Plans'],
    		'ethnicity' => $person['Ethnicity'],
    	];
    }

    public static function truncate()
    {
        DB::connection('hrsql')->statement('
            DELETE from Personal
            DBCC CHECKIDENT (\'Employee_ID\' , RESEED, 0)
        ');
    }
}
