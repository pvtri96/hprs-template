<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Employment extends Model
{
    protected $connection = 'hrsql';
    protected $table = 'Employment';

    protected $fillable = ['Employee_ID', 'Employment_Status', 'Hire_Date', 'Worker_Comp_Code', 'Termination_Date', 'Rehire_Date', 'Last_Review_Date'];

    public function person()
    {
    	return $this->hasOne('App\Model\HRPersonal', 'Employee_ID', 'Employee_ID');
    }


    public static function normalize($employment)
    {
    	return [
    		'employee_id' => $employment['Employee_ID'],
    		'employment_status' => $employment['Employment_Status'],
    		'hire_date' => $employment['Hire_Date'],
    		'worker_comp_code' => $employment['Worker_Comp_Code'],
    		'termination_date' => $employment['Termination_Date'],
    		'rehire_date' => $employment['Rehire_Date'],
    		'last_review_date' => $employment['Last_Review_Date'],
    	];
    }

    public static function truncate()
    {
        DB::connection('hrsql')->statement('
            DELETE from Employment
            DBCC CHECKIDENT (\'Employee_ID\' , RESEED, 0)
        ');
    }
}
