<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	protected $primaryKey = 'permission_id';
    protected $fillable = ['user_id', 'authority_id', 'feature_id', 'status'];
}
