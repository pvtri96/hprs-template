<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Employee extends Model
{
	protected $fillable = ['employee_id', 'first_name', 'last_name', 'middle_name', 'SSN', 'address1', 'address2', 'city', 'state', 'zip', 'email', 'phone_number', 'drivers_license', 'marital_status', 'gender', 'shareholder_status', 'benefit_plans', 'ethnicity', 'pay_rate', 'pay_rates_id_pay_rates', 'vacation_days', 'paid_to_date', 'paid_last_year'];

	private static $employees = null;

    public static function get($columns = ['*'])
    {
    	return static::$employees;
    }

    public static function all($columns = ['*'])
    {
    	$hrs = HRPersonal::all();
    	$prs = PREmployee::all();
    	static::$employees = Collection::make([]);

    	foreach ($prs as $pr) {
    		$hr = static::findEmployeeBySSNFromHR($hrs, $pr['SSN']);
    		static::$employees->push(new Employee(static::normalize($hr, $pr)));
    	}
    	return static::$employees;
    }

    public static function find($SSN)
    {
        $hr = HRPersonal::where('Social_Security_Number', $SSN)->first();
        $pr = PREmployee::where('SSN', $SSN)->first();

        return new Employee(static::normalize($hr, $pr));
    }

    public static function withEmployments()
    {
    	if (static::$employees == null) {
    		static::all();
    	}

    	$employments = Employment::all();

    	foreach (static::$employees as $employee) {
			$employee['employments'] = Collection::make([]);
    		foreach ($employments as $employment) {
    			if ($employment['Employee_ID'] == $employee['employee_id']) {
    				$employee['employments']->push(Employment::normalize($employment));
    			}
    		}
    	}

    	return new self;
    }

    public static function withBenefitPlan()
    {    	
    	if (static::$employees == null) {
    		static::all();
    	}

    	$benefit_plans = BenefitPlan::all();

    	foreach (static::$employees as $employee) {
    		$employee['benefit_plan'] = null;
    		foreach ($benefit_plans as $benefit_plan) {
    			if ($benefit_plan['Benefit_Plan_ID'] == $employee['benefit_plans']) {
    				$employee['benefit_plan'] = BenefitPlan::normalize($benefit_plan);
    			}
    		}
    	}

    	return new self;
    }

    public static function withJobHistories()
    {    	
    	if (static::$employees == null) {
    		static::all();
    	}

    	$job_histories = JobHistory::all();

    	foreach (static::$employees as $employee) {
    		$employee['job_histories'] = Collection::make([]);
    		foreach ($job_histories as $job_history) {
    			if ($job_history['Employee_ID'] == $employee['employee_id']) {
    				$employee['job_histories']->push(JobHistory::normalize($job_history));
    			}
    		}
    	}

    	return new self;
    }

    public static function withPayRate()
    {
    	if (static::$employees == null) {
    		static::all();
    	}

    	$pay_rates = PayRate::all();

    	foreach (static::$employees as $employee) {
    		$employee['pay_rate'] = null;
    		foreach ($pay_rates as $pay_rate) {
    			if ($pay_rate['idPay Rates'] == $employee['pay_rates_id_pay_rates']) {
    				$employee['pay_rate'] = PayRate::normalize($pay_rate);
    			}
    		}
    	}

    	return new self;
    }

    private static function findEmployeeBySSNFromHR($people, $ssn)
    {
    	foreach ($people as $person) {
    		if ($person['Social_Security_Number'] == $ssn)
    			return $person;
    	}
    }

    //Normalize HR and PR to Employee
    public static function normalize($hr, $pr)
    {
    	return [
    		'employee_id' => $hr['Employee_ID'],
    		'SSN' => $hr['Social_Security_Number'],
    		'first_name' => $hr['First_Name'],
    		'last_name' => $hr['Last_Name'],
    		'middle_name' => $hr['Middle_Initial'],
    		'address1' => $hr['Address1'],
    		'address2' => $hr['Address2'],
    		'city' => $hr['City'],
    		'state' => $hr['State'],
    		'zip' => $hr['Zip'],
    		'email' => $hr['Email'],
    		'phone_number' => $hr['Phone_Number'],
    		'drivers_license' => $hr['Drivers_License'],
    		'marital_status' => $hr['Marital_Status'],
    		'gender' => $hr['Gender'],
    		'shareholder_status' => $hr['Shareholder_Status'],
    		'benefit_plans' => $hr['Benefit_Plans'],
    		'ethnicity' => $hr['Ethnicity'],
    		'pay_rate' => $pr['Pay Rate'],
    		'pay_rates_id_pay_rates' => $pr['Pay Rates_idPay Rates'],
    		'vacation_days' => $pr['Vacation Days'],
    		'paid_to_date' => $pr['Paid To Date'],
    		'paid_last_year' => $pr['Paid Last Year'],
    	];
    }
}
